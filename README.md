# React-Redux Example App

```shell
cd src

# install dependencies
npm install

# install & run app with parcel 
npm install -g parcel
parcel index.html
```