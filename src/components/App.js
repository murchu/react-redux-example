import React, { Component } from 'react'
import Clicker from './Clicker'

export default class App extends Component {
    render() {
        return <div id="app-container">
            <h1>My React App Template</h1>
            <p>Welcome to my React app template.</p>
            <Clicker />
        </div>
    }
}