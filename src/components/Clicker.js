import React, { Component } from 'react';
import { connect } from 'react-redux';

class Clicker extends Component {
  render() {
    console.log('props', this.props)
    return (
      <div>
        <p>You've clicked this {this.props.clicks} times!</p>
        <p>
          <button onClick={this.props.increment}>increment</button>
        </p>
      </div>
    );
  }
}

// redux convention - how to read from redux
// make the state from the redux store available as readable props through "this.props"
const mapStateToProps = (state) => {
  console.log('state', state);
  return {
    clicks: state
  };
};

// redux convention - how to write to redux
// make dipspatch available by defining functions that call dispatch
// with an action-like object {type: }
// and these functions become available on "this.props"
const mapDispatchToProps = (dispatch) => {
  return {
    increment: () => {
      console.log('clicked increment');
      dispatch({ type: 'INCREMENT' });
    }
  };
};

// connect class to redux
export default connect(mapStateToProps, mapDispatchToProps)(Clicker);
